<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::group(['prefix'=>'manage', 'namespace'=>'Manager'],function(){


    Route::resource('users', 'UserController');
    Route::resource('categories', 'CategoryController');
});

Auth::routes();
Route::get('/home','HomeController@index')->name('home');
Route::get('/', function () {
    return view('manage.dashboard.index');
});*/
Route::group(['prefix'=>'manage','middleware'=>'auth', 'namespace'=>'Manager'],function(){

    Route::get('/dashboard', function (){
        return view('manage.layout.app');
    });
    Route::resource('users', 'UserController');

    Route::resource('categories', 'CategoryController');

    Route::resource('roles', 'RoleController');


});

Route::get('/', function (){
    return view('welcome');
});
Auth::routes();

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
