<?php


namespace App\Composer;


use App\Models\Cart;
use Illuminate\View\View;

class CartComposer
{
    protected $cart;

    /**
     * CartComposer constructor.
     * @param $cart
     */
    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }


    public function compose(View $view)
    {
        $cartTotal = $this->cart->where('user_id', 1)->count();
        $view->with(['cartTotal' => $cartTotal]);
    }

}
