<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';


    protected $fillable = [
        'name', 'price', 'sale','images'
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_product', 'product_id', 'category_id');
    }

    public function assignCategories($categoryIds = [])
    {
        $this->categories()->sync($categoryIds);
    }

    public function addCategories($categoryIds)
    {
        $this->categories()->attach($categoryIds);
    }

    public function removeCategories($categoryIds)
    {
        $this->categories()->detach($categoryIds);
    }


    // category id =2

//    $product->categories()->detach()
}
