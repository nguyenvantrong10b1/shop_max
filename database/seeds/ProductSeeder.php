<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Product::create([
            'name' =>'quan hao'
        ]);

        \App\Models\Product::create([
            'name' =>'quan hong'
        ]);
        \App\Models\Product::create([
            'name' =>'quan do'
        ]);
        \App\Models\Product::create([
            'name' =>'quan tin'
        ]);

        \App\Models\Product::create([
            'name' =>'quan xanh'
        ]);


        \App\Models\Category::create([
            'name'=> 'quan dep'
        ]);
        \App\Models\Category::create([
            'name'=> 'quan the thao'
        ]);
        \App\Models\Category::create([
            'name'=> 'quan sale'
        ]);

    }
}
