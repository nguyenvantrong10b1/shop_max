@extends('client.layout.app')

@section('title', 'order')

@section('content')


    @if(session('message'))

        <div class="row">
            <div class="alert alert-warning alert-dismissible fade show w-100" role="alert">
                <strong>{{session('message')}} </strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>

    @endif

    @if($carts->count() <1)
        <h3>ban chua co gi</h3>
    @else
        <div class="row">
            <div class="col-4">
                <div class="row">
                    <h3>Thong tin nhan hang</h3>
                </div>

                <div class="row border-1">
                    <form action="{{route('orders.store')}}"  method="post">
                        @csrf
                        <div class="form-group">
                            <lable>Ten</lable>
                            <input type="text" class="form-control" name="customer">
                        </div>
                        <div class="form-group">
                            <lable>dia chi</lable>
                            <input type="text" name="address" class="form-control">
                        </div>
                        <div class="form-group">
                            <lable>so dien thoai</lable>
                            <input type="text" name="phone" class="form-control">
                        </div>
                        <div class="form-check form-check-radio">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="payment"
                                       value="tienmat">
                                thanh toan nhan hang
                                <span class="circle">
                            <span class="check"></span>
                        </span>
                            </label>
                        </div>
                        <div class="form-check form-check-radio">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="payment"
                                       value="card" checked>
                                thanh toan qua the
                                <span class="circle">
                            <span class="check"></span>
                        </span>
                            </label>
                        </div>

                        <button type="submit" class="btn btn-info btn-round">Complete Purchase <i
                                class="material-icons">keyboard_arrow_right</i></button>
                    </form>
                </div>
            </div>
            <div class="col-8">
                <div class="row">
                    <h4>Prodcut detail</h4>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-shopping">
                                        <thead>
                                        <tr>
                                            <th class="text-center"></th>
                                            <th>Product</th>
                                            <th class="th-description">Color</th>
                                            <th class="th-description">Size</th>
                                            <th class="text-right">Price</th>
                                            <th class="text-right">Qty</th>
                                            <th class="text-right">Amount</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($carts as $item)
                                            <tr id="cart-box-{{$item->id}}">
                                                <td>
                                                    <div class="img-container">
                                                        <img src="../../assets/img/product1.jpg" alt="...">
                                                    </div>
                                                </td>
                                                <td class="td-name">
                                                    <a href="#{{$item->product->name}}">{{$item->product->name}}</a>
                                                    <br>
                                                    <small>by Dolce&amp;Gabbana</small>
                                                </td>
                                                <td>
                                                    Red
                                                </td>
                                                <td>
                                                    M
                                                </td>
                                                <td class="td-number text-right">
                                                    <small>$</small>{{$item->product->price}}
                                                </td>
                                                <td class="td-number">
                                                    <span id="quantity{{$item->id}}">{{$item->quantity}}</span>
                                                    <div class="btn-group btn-group-sm">
                                                        <button class="btn btn-round btn-info change-quantity"
                                                                data-quantity="-1"
                                                                data-product_id="{{$item->product->id}}"
                                                                data-action="{{route('cart.change_quantity')}}"><i
                                                                class="material-icons">remove</i></button>
                                                        <button class="btn btn-round btn-info change-quantity"
                                                                data-quantity="+1"
                                                                data-product_id="{{$item->product->id}}"
                                                                data-action="{{route('cart.change_quantity')}}"><i
                                                                class="material-icons">add</i></button>
                                                    </div>
                                                </td>
                                                <td class="td-number">
                                                    <small>$</small><span
                                                        id="total{{$item->id}}">{{$item->total}}</span>
                                                </td>
                                                <td class="td-actions">
                                                    <button type="button" rel="tooltip" data-placement="left" title=""
                                                            class="btn btn-link" data-original-title="Remove item">
                                                        <i class="material-icons">close</i>
                                                    </button>
                                                </td>
                                            </tr>

                                        @endforeach

                                        <tr>
                                            <td colspan="5">
                                                <form action="{{route('orders.add_coupon')}}" method="post">
                                                    <lable>coupon code</lable>
                                                    @csrf
                                                    <input name="code" type="text" placeholder="code"
                                                           class="text-uppercase">
                                                    <button>Apply</button>
                                                </form>

                                            </td>
                                            <td colspan="3" class="text-center">
                                                Total:
                                                <small>$</small> {{$carts->sum('total')}}

                                                {{ session()->has('discount') ?'-'. session('discount') :'' }}

                                                {{ session()->has('totalAfterDiscount') ?'='. session('totalAfterDiscount') :'' }}

                                                <span>{{ session()->has('code') ?'ban da apply code:'. session('code') :'' }}</span>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif




@endsection

@section('script')

    <script>
        $(() => {
            $(document).on('click', '.change-quantity', function (event) {
                event.preventDefault();

                let product_id = $(this).data('product_id');
                let url = $(this).data('action');
                // let quantity = $(this).data('quantity');
                let quantity = $(this).data('quantity');

                $.ajax({
                    url: url,
                    data: {
                        product_id,
                        quantity
                    },
                    method: 'post'
                }).then((response) => {
                    let {data} = response;
                    if (data.quantity <= 0) {
                        $(`#cart-box-${data.id}`).remove();
                    } else {
                        $(`#quantity${data.id}`).text(data.quantity)
                        $(`#total${data.id}`).text(+data.quantity * (+data.product.price))
                    }
                })
                    .catch(error => {

                    })
            });
        })

    </script>
@endsection
