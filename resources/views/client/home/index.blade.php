@extends('client.layout.app')

@section('title', 'cart')

@section('content')

    <div class="row">
        <h3>Cart</h3>

        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">assignment</i>
                    </div>
                    <h4 class="card-title">Shopping Cart Table</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-shopping">
                            <thead>
                            <tr>
                                <th class="text-center"></th>
                                <th>Product</th>
                                <th class="th-description">Color</th>
                                <th class="th-description">Size</th>
                                <th class="text-right">Price</th>
                                <th class="text-right">Qty</th>
                                <th class="text-right">Amount</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($carts as $item)
                                <tr id="cart-box-{{$item->id}}">
                                    <td>
                                        <div class="img-container">
                                            <img src="../../assets/img/product1.jpg" alt="...">
                                        </div>
                                    </td>
                                    <td class="td-name">
                                        <a href="#{{$item->product->name}}">{{$item->product->name}}</a>
                                        <br>
                                        <small>by Dolce&amp;Gabbana</small>
                                    </td>
                                    <td>
                                        Red
                                    </td>
                                    <td>
                                        M
                                    </td>
                                    <td class="td-number text-right">
                                        <small>$</small>{{$item->product->price}}
                                    </td>
                                    <td class="td-number">
                                        <span id="quantity{{$item->id}}">{{$item->quantity}}</span>
                                        <div class="btn-group btn-group-sm">
                                            <button class="btn btn-round btn-info change-quantity"
                                                    data-quantity="-1"
                                                    data-product_id="{{$item->product->id}}"
                                                    data-action="{{route('cart.change_quantity')}}"><i
                                                    class="material-icons">remove</i></button>
                                            <button class="btn btn-round btn-info change-quantity"
                                                    data-quantity="+1"
                                                    data-product_id="{{$item->product->id}}"
                                                    data-action="{{route('cart.change_quantity')}}"><i
                                                    class="material-icons">add</i></button>
                                        </div>
                                    </td>
                                    <td class="td-number">
                                        <small>$</small><span
                                            id="total{{$item->id}}">{{round($item->product->price * $item->quantity, 2)}}</span>
                                    </td>
                                    <td class="td-actions">
                                        <button type="button" rel="tooltip" data-placement="left" title=""
                                                class="btn btn-link" data-original-title="Remove item">
                                            <i class="material-icons">close</i>
                                        </button>
                                    </td>
                                </tr>

                            @endforeach
                            <tr>
                                <td colspan="6"></td>
                                <td colspan="2" class="text-right">
                                    <button type="button" class="btn btn-info btn-round">Complete Purchase <i
                                            class="material-icons">keyboard_arrow_right</i></button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script>
        $(() => {
            $(document).on('click', '.change-quantity', function (event) {
                event.preventDefault();

                let product_id = $(this).data('product_id');
                let url = $(this).data('action');
                // let quantity = $(this).data('quantity');
                let quantity = $(this).data('quantity');

                $.ajax({
                    url: url,
                    data: {
                        product_id,
                        quantity
                    },
                    method: 'post'
                }).then((response) => {
                    let {data} = response;
                    if (data.quantity == 0) {
                        $(`#cart-box-${data.id}`).remove();
                    } else {
                        $(`#quantity${data.id}`).text(data.quantity)
                        $(`#total${data.id}`).text(+data.quantity * (+data.product.price))
                    }
                })
                    .catch(error => {

                    })
            });


        })

    </script>
@endsection
