@extends('manage.layout.app')

@section('title', 'Users')

@section('content')

    @if(session('message'))
        <h1>{{session('message')}} </h1>
    @endif
    <a class="btn btn-primary" href="{{route('categories.create')}}">Create User</a>

    <h1>LIST </h1>

    <table  class="table table-bordered">
        <tr>
            <td>STT</td>
            <td>name</td>
            <td>parent name</td>
            <td>action</td>
        </tr>

        @foreach($categories as $item)
            <tr>

                <td>{{$loop->iteration}}</td>
                <td>{{$item->name}}</td>
                {{--                <td>{{$item->parentCategories ? $item->parentCategories->name : 'Khong co cha'}}</td>--}}
                <td>{{optional($item->parentCategories)->name ?? 'Khong co cha'}}</td>
                {{--                <td>{{$item->parentCategories?->name ?? 'Khong co cha'}}</td>--}}
                <td>
                    <a class="btn btn-warning text-white" href="{{route('categories.edit', $item->id)}}">Edit</a>
                    <form action="{{route('categories.destroy', $item->id)}}"  method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">DELETE</button>

                    </form>
                </td>
            </tr>
        @endforeach
    </table>


    <div>
        {{ $categories   ->links() }}
    </div>

@endsection
