@extends('manage.layout.app')

@section('title', 'orders')

@section('content')
    <h1>LIST ORDER </h1>

    <table  class="table table-bordered">
        <tr>
            <td>STT</td>
            <td>customer name</td>
            <td>Address</td>
            <td>phone</td>
            <td>Payment</td>
            <td>Status</td>
            <td>Total</td>
            <td>Discount</td>
            <td>Date</td>
            <td>Action</td>
        </tr>

        @foreach($orders as $item)
            <tr>

                <td>{{$loop->iteration}}</td>
                <td>{{$item->customer}}</td>
                <td>{{$item->address}}</td>
                <td>{{$item->phone}}</td>
                <td>{{$item->payment}}</td>
                <td>{{$item->status}}</td>
                <td>{{$item->total}}</td>
                <td>{{$item->discount}}</td>
                <td>{{$item->order_create}}</td>
                <td>
                    <a class="btn btn-round btn-just-icon btn-info" href="{{route('manage.orders.show', $item->id)}}">
                        <i class="material-icons">visibility</i>

                    </a>
                </td>
            </tr>
        @endforeach
    </table>


    <div>
        {{ $orders   ->links() }}
    </div>

@endsection
