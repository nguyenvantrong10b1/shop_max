@extends('manage.layout.app')

@section('title', 'Users')

@section('content')

    @if(session('message'))
        <h1>{{session('message')}} </h1>
    @endif
    <a class="btn btn-primary" href="{{route('users.create')}}">Create User</a>

    <h1>LIST </h1>

    <table  class="table table-bordered">
        <tr>
            <td>STT</td>
            <td>name</td>
            <td>tole</td>
            <td>image</td>
            <td>action</td>
        </tr>

        @foreach($users as $user)
            <tr>

                <td>{{$loop->iteration}}</td>
                <td>{{$user->name}}</td>
                <td>
                    @if($user->roles->count() >0)
                        @foreach($user->roles as $role)
                            {{$role->display_name .','}}
                        @endforeach
                    @endif
                </td>
                <td>
                    <img width="100px" height="100px" src="{{asset('upload/'.$user->image)}}" alt="">
                </td>
                <td>
                    <a class="btn btn-warning text-white" href="{{route('users.edit', $user->id)}}">Edit</a>

                    <form action="{{route('users.destroy', $user->id)}}"  method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">DELETE</button>

                    </form>
                </td>
            </tr>
        @endforeach
    </table>


    <div>
        {{ $users->links() }}
    </div>

@endsection
